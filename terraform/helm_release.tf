provider "helm" {
  kubernetes {
    host = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
      command     = "aws"
    }
    
  }

}

resource "helm_release" "microservice" {
  name       = "my-local-chart"
  chart      = "../charts/microservice"
  values = [
    file("../values/ad-service-values.yaml"),
    file("../values/cart-service-values.yaml"),
    file("../values/checkout-service-values.yaml"),
    file("../values/currency-service-values.yaml"),
    file("../values/email-service-values.yaml"),
    file("../values/frontend-values.yaml"),
    file("../values/payment-service-values.yaml"),
    file("../values/productcatalog-service-values.yaml"),
    file("../values/recommendation-service-values.yaml"),
    file("../values/shipping-service-values.yaml")

  ]
}
resource "helm_release" "redis" {
  name       = "my-chart-release"
  chart      = "../charts/redis"

  values = [
    file("../charts/redis/values.yaml")
  ]
}
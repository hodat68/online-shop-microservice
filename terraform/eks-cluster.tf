provider "kubernetes" {

    host = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      command     = "aws"
      args = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
    }
}


# data "aws_eks_cluster" "app-cluster" {
#     name = "microservice-app-eks-cluster"
# }

# data "aws_eks_cluster_auth" "app-cluster" {
#     name = "microservice-app-eks-cluster"
# }

module "eks" {
   source = "terraform-aws-modules/eks/aws"
   version = "19.15.3"

   cluster_name =  "microservice-app-eks-cluster"
   cluster_version = "1.27"

   cluster_endpoint_public_access  = true

   subnet_ids = module.microservice-app-vpc.private_subnets
   vpc_id = module.microservice-app-vpc.vpc_id

   tags = {
        environment = "development"
        application = "microserviceapp"
    }

  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"

  }

  eks_managed_node_groups = {
    one = {
      name = "node-group-1"

      instance_types = ["t2.small"]

      min_size     = 1
      max_size     = 3
      desired_size = 2
    }

    two = {
      name = "node-group-2"

      instance_types = ["t2.small"]

      min_size     = 1
      max_size     = 2
      desired_size = 1
    }
  }


  
}

output "output_eks" {
  value = module.eks.cluster_endpoint
}

# module "helm_chart" {
#   source = "../terraform-helm"
#   cluster_name = module.eks.output_eks.cluster_name
#   cluster_endpoint = module.eks.output_eks
#   cluster_ca_cert = base64decode(module.eks.output_eks.cluster_certificate_authority_data)
  
# }

# resource "null_resource" "kubeconfig_generation" {
#   provisioner "local-exec" {
#     command = <<-EOT
#       aws eks update-kubeconfig --name ${module.eks.cluster_name} --region ap-southeast-1 --kubeconfig kubeconfig
#     EOT
#   }
# }

# data "local_file" "helmfile_yaml" {
#   filename = "D:/Learn_DevOps/online-shop-microservices/helmfile.yaml"
# }

# resource "null_resource" "helmfile_execution" {
#   depends_on = [null_resource.kubeconfig_generation]
#   provisioner "local-exec" {
#     command = "helmfile sync"
#     working_dir = "D:/Learn_DevOps/online-shop-microservices"
#     environment = {
#       HELM_HOME = "D:/Learn_DevOps/online-shop-microservices/charts"
#       KUBECONFIG = "kubeconfig"
#     }
#   }

#   triggers = {
#     exit_code = null
#   }

# }

# output "helmfile_execution_exit_code" {
#   value = null_resource.helmfile_execution.triggers.exit_code
# }

